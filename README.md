# Attributes Proxy

## Running locally

Install poetry via:

```sh
pipx install poetry && pipx inject poetry poethepoet[poetry_plugin]
```

Make a copy of the example configuration:
```sh
cp config.env.example config.env
```

Install application dependencies via:

```sh
poetry install
```
Start the application via:

```sh
poetry poe up
```

Press Ctrl-C to stop the application.

> **TIP:** You can see all configured `poe` tasks by just running the `poe`
> command with no options.

The application can be stopped via

```sh
poetry poe down
```

Logs can be streamed via:

```sh
docker compose logs -f
```

### Configuration

The configuration options are specified in the `config.env.example`. Configuration items are read
from the environment variables. These are read in using [`pydantic-settings`](https://docs.pydantic.dev/latest/concepts/pydantic_settings/).

### Client Ids

The `CLIENT_IDS` configuration item must be provided. This configuration item specifies a list of
acceptable client id strings.

For the log to be accepted when client id checking is enabled clients must provide the header
`X-Ucam-Observe-Client-Id` set to one of the strings specified in the server's `CLIENT_IDS`
configuration.

If the `CLIENT_IDS` configuration item is an empty list then client id checking is disabled.

## Local development

Install git pre-commit hooks via:

```console
poetry run pre-commit install
```

View all the available poe tasks by running:
```console
poe
```

Start the application _from the
production container_:

```console
poetry poe up:production
```

To run pre-commit checks in order to fix up code style and layout to match our
common convention:

```console
poetry poe fix
```

To build or rebuild all container images used by the application:

```console
poetry poe compose:build
```

To pull any images used by the docker compose configuration:

```console
poetry poe compose:pull
```

### Dependencies

> **IMPORTANT:** if you add a new dependency to the application as described
> below you will need to run `docker compose build` or add `--build` to the
> `docker compose run` and/or `docker compose up` command at least once for
> changes to take effect when running code inside containers. The poe tasks have
> already got `--build` appended to the command line.

To add a new dependency _for the application itself_:

```console
poetry add {dependency}
```

To add a new development-time dependency _used only when the application is
running locally in development or in testing_:

```console
poetry add -G dev {dependency}
```

To remove a dependency which is no longer needed:

```console
poetry remove {dependency}
```

### Stress Testing

This project also has some stress tests set up which run as part of the pytest
tests. These can be run using a `poe` command:

```console
poetry poe stress
```

Note that the stress tests do not bring the server up with them - so the project
must have been started separately (for example using `poetry poe up`).

#### Stress Test Configuration

The stress tests are primarily configured through the docker compose file. The
`stress_test` service in the `docker-compose.yml` file contains details for how
the stress tests can be configured.

## CI configuration

The project is configured with Gitlab AutoDevOps via Gitlab CI using the .gitlab-ci.yml file.
