import importlib
import logging
from unittest import mock

import pytest
from fastapi.testclient import TestClient

import ucam_observe_remote_server.main as main_module


@pytest.fixture
def client():
    importlib.reload(main_module)
    # Need to also re-create the client with the newly reloaded main_module
    return TestClient(main_module.app)


@pytest.fixture
def set_limiter_envs(monkeypatch):
    monkeypatch.setenv("USE_LIMITERS", "True")
    monkeypatch.setenv("MAX_REQUESTS_PER_MINUTE", "1")


@pytest.fixture
def set_global_limiter_envs(monkeypatch):
    monkeypatch.setenv("USE_LIMITERS", "True")
    monkeypatch.setenv("MAX_GLOBAL_REQUESTS_PER_DAY", "1")


@pytest.fixture
def set_client_id_envs(monkeypatch):
    monkeypatch.setenv("CLIENT_IDS", '["test-client-id"]')


# Prefer patching logger rather than verifying output, as internal details of how the ucam-observe
# library writes logs may change - and subsequently break our tests. In this case we will verify we
# call the function with expected arguments.
@mock.patch("ucam_observe_remote_server.main.logger")
def test_accept_log(mock_logger, client):
    response = client.post("/log", json={"log_level": "info", "event": "Oh no!"})
    assert response.status_code == 200
    assert response.text == "ok"

    mock_logger.log.assert_called_once_with(
        logging.INFO,
        "remote_log",
        labels={"log_type": "remote", "authenticated": False},
        remote_reported_event="Oh no!",
    )


@mock.patch("ucam_observe_remote_server.main.logger")
def test_accept_log_with_additional_data(mock_logger, client):
    response = client.post(
        "/log",
        json={"log_level": "info", "event": "Oh no!", "data": {"more": ["info", "to", "log"]}},
    )
    assert response.status_code == 200
    assert response.text == "ok"

    mock_logger.log.assert_called_once_with(
        logging.INFO,
        "remote_log",
        labels={"log_type": "remote", "authenticated": False},
        remote_reported_event="Oh no!",
        remote_reported_data={"more": ["info", "to", "log"]},
    )


@mock.patch("ucam_observe_remote_server.main.logger")
def test_accept_log_with_trace_id(mock_logger, client):
    response = client.post(
        "/log",
        json={"log_level": "info", "event": "Oh no!"},
        headers={"X-Cloud-Trace-Context": "my-trace-id"},
    )
    assert response.status_code == 200
    assert response.text == "ok"

    mock_logger.log.assert_called_once_with(
        logging.INFO,
        "remote_log",
        labels={"log_type": "remote", "authenticated": False},
        remote_reported_event="Oh no!",
        trace_id="my-trace-id",
    )


def test_accept_log_no_content_length(client):
    response = client.post(
        "/log", json={"log_level": "info", "event": "Oh no!"}, headers={"Content-Length": "0"}
    )
    assert response.status_code == 411


def test_accept_log_content_length_too_long(client):
    response = client.post(
        "/log", json={"log_level": "info", "event": "Oh no!"}, headers={"Content-Length": "128001"}
    )
    assert response.status_code == 413
    assert response.json()["detail"] == "Content-Length too large"


def test_accept_log_payload_too_large(client):
    response = client.post(
        "/log",
        # Valid JSON - but much too big!
        json={"log_level": "info", "event": "1" * 128000},
        headers={"Content-Length": "128000"},
    )
    assert response.status_code == 413
    assert response.json()["detail"] == "Payload too large"


def test_limiter(set_limiter_envs, client):
    response = client.post("/log", json={"log_level": "info", "event": "Oh no!"})
    assert response.status_code == 200

    response = client.post("/log", json={"log_level": "info", "event": "Oh no!"})
    assert response.status_code == 429


def test_global_limiter(set_global_limiter_envs, client):
    response = client.post("/log", json={"log_level": "info", "event": "Oh no!"})
    assert response.status_code == 200

    response = client.post("/log", json={"log_level": "info", "event": "Oh no!"})
    assert response.status_code == 429


def test_with_client_id(set_client_id_envs, client):
    response = client.post("/log", json={"log_level": "info", "event": "Oh no!"})
    assert response.status_code == 400

    response = client.post(
        "/log",
        json={"log_level": "info", "event": "Oh no!"},
        headers={
            "x-ucam-observe-client-id": "test-client-id",
        },
    )
    assert response.status_code == 200
