import typing

from fastapi import Depends, FastAPI, Header, HTTPException, Request, Response
from fastapi.middleware.cors import CORSMiddleware

# Importing this internal function is recommended by the documentation :(
# See: https://slowapi.readthedocs.io/en/latest/
from slowapi import Limiter, _rate_limit_exceeded_handler
from slowapi.errors import RateLimitExceeded
from slowapi.middleware import SlowAPIMiddleware
from slowapi.util import get_remote_address
from ucam_observe import get_structlog_logger
from uvicorn.middleware.proxy_headers import ProxyHeadersMiddleware

from .config import Settings
from .models import LogLevel, RemoteLog

settings = Settings()
app = FastAPI()

logger = get_structlog_logger(__name__)

# Maximum payload size is 2x the maximum accepted combined size of the URL and header of any
# request to a GCP Load Balancer - see:
# https://cloud.google.com/load-balancing/docs/quotas#https-lb-header-limits
MAX_PAYLOAD_SIZE_BYTES = 128000


# Set up limiters based on settings values
limiter = Limiter(
    key_func=get_remote_address, enabled=settings.use_limiters, storage_uri=settings.limit_storage
)
app.state.limiter = limiter

app.add_exception_handler(
    RateLimitExceeded,
    # The slowapi _rate_limit_exceeded_handler is specified with type hints incompatible
    # with starlette's add_exception_handler function. This lambda avoids mypy throwing an
    # error.
    lambda request, exc: _rate_limit_exceeded_handler(
        request, typing.cast(RateLimitExceeded, exc)
    ),
)

app.add_middleware(
    CORSMiddleware,
    allow_origins=(
        settings.cors_origins
        if isinstance(settings.cors_origins, list)
        else [settings.cors_origins]
    ),
    allow_credentials=True,
    allow_methods=["POST", "GET"],
    allow_headers=["*"],
)
app.add_middleware(SlowAPIMiddleware)
app.add_middleware(ProxyHeadersMiddleware, trusted_hosts=settings.proxy_trusted_hosts)


async def check_payload_size(
    request: Request, content_length: typing.Annotated[int | None, Header()] = None
):
    """
    Dependency to check the payload content-length is set correctly and payload size is also within
    maximum.
    """
    if not content_length:
        raise HTTPException(411, detail="Content-Length required")
    if content_length > MAX_PAYLOAD_SIZE_BYTES:
        raise HTTPException(413, detail="Content-Length too large")

    # Do this via stream(), so that if the payload is very large it doesn't all get immediately
    # loaded into memory.
    total_size = 0
    async for chunk in request.stream():
        total_size += len(chunk)
        if total_size > MAX_PAYLOAD_SIZE_BYTES:
            raise HTTPException(413, detail="Payload too large")


def check_client_id(x_ucam_observe_client_id: typing.Annotated[str, Header()] = ""):
    """
    Check the client id header is correct.
    """
    if not settings.client_ids:
        return
    if x_ucam_observe_client_id in settings.client_ids:
        return
    raise HTTPException(400, detail="Invalid client id")


@app.post(
    "/log",
    response_class=Response,
    dependencies=[Depends(check_payload_size), Depends(check_client_id)],
)
@limiter.limit(f"{settings.max_requests_per_minute}/minute")
@limiter.limit(f"{settings.max_global_requests_per_day}/day", key_func=lambda: "global")
async def accept_log(
    request: Request,
    remote_log: RemoteLog,
    x_cloud_trace_context: typing.Annotated[str, Header()] = "",
):
    level = LogLevel.to_logging(remote_log.log_level)
    log_information = {
        "labels": {"log_type": "remote", "authenticated": False},
        "remote_reported_event": remote_log.event,
    }
    if remote_log.data:
        log_information["remote_reported_data"] = remote_log.data
    if x_cloud_trace_context:
        log_information["trace_id"] = x_cloud_trace_context

    logger.log(level, "remote_log", **log_information)
    return Response(content="ok", status_code=200)


@app.get("/healthy", response_class=Response)
def read_status():
    return Response(content="ok", status_code=200)
