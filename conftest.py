import os

import httpx
import pytest


def pytest_configure(config):
    """
    The pytest configuration sets up the `stress` marker, and reads the stress test environment
    variables to select/deselect stress tests.
    """
    # register an additional marker
    config.addinivalue_line("markers", "stress: mark test as stress test")

    if not os.environ.get("STRESS_TEST_URL", None):
        if getattr(config.option, "markexpr", None):
            config.option.markexpr += "and not stress"
        else:
            setattr(config.option, "markexpr", "not stress")
    else:
        setattr(config.option, "markexpr", "stress")


@pytest.fixture
def stress_client(pytestconfig):
    """
    Fixture for providing a client to make requests for stress testing. This can only be used in
    tests marked with `pytest.mark.stress`.
    """
    stress_test_url = os.environ.get("STRESS_TEST_URL", None)
    if not stress_test_url:
        raise RuntimeError("Stress client requested in normal test")
    if client_id := os.environ.get("STRESS_TEST_CLIENT_ID", None):
        headers = {"x-ucam-observe-client-id": client_id}
    else:
        headers = {}
    with httpx.Client(base_url=stress_test_url, headers=headers) as client:
        yield client
